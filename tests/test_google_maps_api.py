from requests import Response
from utils.api import GoogleMapsAPI
from utils.checking import Checking
import json

"""Создание,изменение и удаление новой локации"""
class TestCreatePlace():
    

    def test_create_new_place(self):
        print("Метод POST...")
        result_post: Response = GoogleMapsAPI.create_new_place()
        
        check_post = result_post.json()
        place_id = check_post.get("place_id")
        Checking.check_status_code(response=result_post, status_code=200)
        Checking.check_json_token(result_post, ['status', 'place_id', 'scope', 'reference', 'id'])
        token = json.loads(result_post.text)
        print(list(token))
        Checking.check_json_value(result_post, 'status', 'OK')
        
        print("\nМетод get POST...")
        result_get: Response = GoogleMapsAPI.get_new_place(place_id)
        Checking.check_status_code(response=result_get, status_code=200)
        Checking.check_json_token(result_get, ['location', 'accuracy', 'name', 'phone_number', 'address', 'types', 'website', 'language'])
        token = json.loads(result_get.text)
        print(list(token))
        Checking.check_json_value(result_get, 'phone_number', '(+91)9838933937')
        
        # print("\nМетод put...")
        # result_put: Response = GoogleMapsAPI.put_new_place(place_id)
        # Checking.check_status_code(response=result_put, status_code=200)
        # Checking.check_json_token(result_put, ['msg'])
        # token = json.loads(result_put.text)
        # print(list(token))
        
        # print("\nМетод get PUT...")
        # result_get: Response = GoogleMapsAPI.get_new_place(place_id)
        # Checking.check_status_code(response=result_get, status_code=200)
        # Checking.check_json_token(result_get, ['location', 'accuracy', 'name', 'phone_number', 'address', 'types', 'website', 'language'])
        
        # print("\nМетод delete...")
        # result_delete: Response = GoogleMapsAPI.delete_place(place_id)
        # Checking.check_status_code(response=result_delete, status_code=200)
        # Checking.check_json_token(result_delete, ['status'])
        
        # print("\nМетод get DELETE...")
        # result_get: Response = GoogleMapsAPI.get_new_place(place_id)
        # Checking.check_status_code(response=result_get, status_code=404)
        # Checking.check_json_token(result_get, ['msg'])
        