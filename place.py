import requests

class TestNewLocation():

    def test_create_new_location(self):
        """Создание новой локации"""
        base_url = "https://rahulshettyacademy.com" # базовый url
        post_resource = "/maps/api/place/add/json" # ресурс
        key = "?key=qaclick123" # параметр для всех запросов

        post_url = base_url + post_resource + key

        json_for_create_new_location = {
            "location": {
            "lat":-38.383494,
            "lng":33.427362
            },"accuracy":50,
            "name":"Frontlinehouse",
            "phone_number":"(+91)9838933937",
            "address":"29,sidelayout,cohen09",
            "types":[
            "shoepark",
            "shop"
            ],
            "website":"http://google.com",
            "language":"French-IN"
        }
        
        result_post = requests.post(post_url, json=json_for_create_new_location)
        print(result_post.text)
        
        if 200 == result_post.status_code:
            print("Успешно! Создана новая локация.")
        else:
            print("Провал")
        
        check_post = result_post.json()
        check_info_post = check_post.get("status")
        print(f"Status: {check_info_post}")
        assert check_info_post == "OK"
        
        place_id = check_post.get("place_id")
        print(f"place_id: {place_id}")
        
        
        """Проверка создания новой локации"""
        get_resource = "/maps/api/place/get/json"
        get_url = base_url + get_resource + key + "&place_id="+place_id
        result_get = requests.get(get_url)
        print(result_get.text)
        
        
    def update_address(self):
        """Изменение новой локации"""
        base_url = "https://rahulshettyacademy.com" # базовый url
        put_resource = "/maps/api/place/update/json" # ресурс
        key = "?key=qaclick123"
        put_url = base_url + put_resource + key
        request_body = {
            "place_id":"33ace1f0361a0a3bbacd035c6cb0b01f",
            "address":"100 Lenina street, RU",
            "key":"qaclick123"
        }
        
        result_put = requests.put(put_url, json=request_body)
        print(result_put.text)
        self.get_location_by_place_id("33ace1f0361a0a3bbacd035c6cb0b01f")
        
        
    def get_location_by_place_id(self, place_id):
        """Проверка локации по place_id"""
        print("\nПроверка локации по place_id...")
        
        base_url = "https://rahulshettyacademy.com"
        get_resource = "/maps/api/place/get/json"
        key = "?key=qaclick123"
        get_url = base_url + get_resource + key + "&place_id="+place_id
        result_get = requests.get(get_url)
        print(result_get.text)
        

        
        

place = TestNewLocation()
place.get_location_by_place_id("33ace1f0361a0a3bbacd035c6cb0b01f")