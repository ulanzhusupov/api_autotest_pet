from utils.http_method import HttpMethod

base_url = "https://rahulshettyacademy.com"
key = "?key=qaclick123"


"""Методы для тестировани GoogleMapsApi"""
class GoogleMapsAPI:
    
    # Метод для создания новой локации
    @staticmethod
    def create_new_place():
        json_for_create_new_place = {
            "location":{
            "lat":-38.383494,
            "lng":33.427362
            },"accuracy":50,
            "name":"Frontlinehouse",
            "phone_number":"(+91)9838933937",
            "address":"29,sidelayout,cohen09",
            "types":[
            "shoepark",
            "shop"
            ],
            "website":"http://google.com",
            "language":"French-IN"
        }
        # Ресурс метода post
        post_resource = "/maps/api/place/add/json"
        post_url = base_url + post_resource + key
        
        result_post = HttpMethod.post(post_url, json_for_create_new_place)
        print(result_post.text)
        
        return result_post
    
    
    # Метод для создания новой локации
    @staticmethod
    def get_new_place(place_id):
        get_resource = "/maps/api/place/get/json"
        get_url = base_url + get_resource + key + '&place_id=' + place_id
        print(get_url)
        
        result_get = HttpMethod.get(get_url)
        print(result_get.text)
        
        return result_get
    
    
    # Метод для изменения новой локации
    @staticmethod
    def put_new_place(place_id):
        put_resource = "/maps/api/place/update/json"
        put_url = base_url + put_resource + key + '&place_id=' + place_id
        print(put_url)
        
        json_for_update_location = {
            "place_id":f"{place_id}",
            "address":"100 Lenina street, RU",
            "key":"qaclick123"
        }
        
        result_put = HttpMethod.put(put_url, json_for_update_location)
        print(result_put.text)
        
        return result_put

    
    # Метод для удаления локации
    @staticmethod
    def delete_place(place_id):
        delete_resource = "/maps/api/place/delete/json"
        delete_url = base_url + delete_resource + key
        print(delete_url)
        
        json_for_update_location = {
            "place_id":f"{place_id}",
        }
        
        result_delete = HttpMethod.delete(delete_url, json_for_update_location)
        print(result_delete.text)
        
        return result_delete